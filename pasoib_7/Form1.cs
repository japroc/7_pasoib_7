﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using PcapDotNet.Base;
using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.Ethernet;
using PcapDotNet.Packets.Icmp;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;

namespace pasoib_7
{
    public partial class Form1 : Form
    {
        IList<LivePacketDevice> Devs;
        List<String> packet_names;
        List<byte[]> packet_datum;

        public Form1()
        {
            InitializeComponent();
            InitDevicesComboBox();
            comboBox_Eth_Proto.SelectedIndex = 0;
            comboBox_IP_Version.SelectedIndex = 0;
            comboBox_IP_Proto.SelectedIndex = 0;
            radioButton_ICMP_EchoRequest.Checked = true;
            packet_names = new List<String>();
            packet_datum = new List<byte[]>();
        }

        void InitDevicesComboBox()
        {
            Devs = GetDevices();
            foreach (LivePacketDevice Dev in Devs)
            {
                comboBox_Devices.Items.Add(Dev.Description + '(' + Dev.Addresses[1].Address + ')');
            }
            comboBox_Devices.SelectedIndex = 0;
        }

        static IList<LivePacketDevice> GetDevices()
        {
            // Retrieve the device list from the local machine
            IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;
            return allDevices;
        }

        private EthernetLayer BuildEthernetLayer()
        {
            EthernetType Proto;
            switch (comboBox_Eth_Proto.SelectedIndex)
            {
                case 0: Proto = EthernetType.IpV4; break;
                case 1: Proto = EthernetType.IpV6; break;
                case 2: Proto = EthernetType.Arp; break;
                default: Proto = EthernetType.None; break;
            };

            EthernetLayer ethernetLayer =
                new EthernetLayer
                {
                    Source = new MacAddress(textBox_Eth_Source.Text),
                    Destination = new MacAddress(textBox_Eth_Dest.Text),
                    EtherType = Proto
                };

            return ethernetLayer;
        }

        private Packet BuildEthernetPacket()
        {
            EthernetLayer ethernetLayer = BuildEthernetLayer();

            PayloadLayer payloadLayer =
                new PayloadLayer
                {
                    Data = new Datagram(Encoding.ASCII.GetBytes(textBox_Eth_Payload.Text)),
                };

            PacketBuilder builder = new PacketBuilder(ethernetLayer, payloadLayer);

            return builder.Build(DateTime.Now);
        }

        private void button_Eth_Click(object sender, EventArgs e)
        {
            SendPacket(BuildEthernetPacket());
        }

        private void SendPacket(Packet packet)
        {
            PacketDevice Dev = Devs[comboBox_Devices.SelectedIndex];
            PacketCommunicator communicator = Dev.Open(
                100, // name of the device
                PacketDeviceOpenAttributes.Promiscuous, // promiscuous mode
                1000); // read timeout

            communicator.SendPacket(packet);
        }

        private void button_IP_Send_Click(object sender, EventArgs e)
        {
            SendPacket(BuildIPPacket());
        }

        private ILayer BuildIPLayer()
        {
            byte TOS = 0;
            bool found_proto = true;
            IpV4Protocol Proto = IpV4Protocol.Ip;
            IpV4Layer ipV4Layer;

            switch (comboBox_IP_Proto.SelectedIndex)
            {
                case 0: Proto = IpV4Protocol.Tcp; break;
                case 1: Proto = IpV4Protocol.Udp; break;
                default: Proto = IpV4Protocol.InternetControlMessageProtocol; break;
            }

            if (checkBox_IP_1bit.Checked) TOS |= 0x80;
            if (checkBox_IP_2bit.Checked) TOS |= 0x40;
            if (checkBox_IP_3bit.Checked) TOS |= 0x20;
            if (checkBox_IP_D.Checked) TOS |= 0x10;
            if (checkBox_IP_T.Checked) TOS |= 0x08;
            if (checkBox_IP_R.Checked) TOS |= 0x04;
            if (checkBox_IP_C.Checked) TOS |= 0x02;
            if (checkBox_IP_X.Checked) TOS |= 0x01;

            ipV4Layer = new IpV4Layer
            {
                Source = new IpV4Address(textBox_IP_Src.Text),
                CurrentDestination = new IpV4Address(textBox_IP_Dest.Text),
                Fragmentation = IpV4Fragmentation.None,
                HeaderChecksum = null, // Will be filled automatically.
                Identification = UInt16.Parse(textBox_IP_Identification.Text),
                Options = IpV4Options.None,
                Protocol = null, // Will be filled automatically.
                Ttl = (byte)(UInt32.Parse(textBox_IP_TTL.Text)),
                TypeOfService = TOS,
            };

            if (found_proto) 
                ipV4Layer.Protocol = Proto;

            if (textBox_IP_Checksum.Text.Length > 0) 
                ipV4Layer.HeaderChecksum = UInt16.Parse(textBox_IP_Checksum.Text);

            ushort off = 0;
            if (textBox_IP_FragmentOffset.Text.Length > 0)
            {
                off = UInt16.Parse(textBox_IP_FragmentOffset.Text);
            }
            IpV4FragmentationOptions Opt = 0;
            if (checkBox_IP_DF.Checked) Opt |= IpV4FragmentationOptions.DoNotFragment;
            if (checkBox_IP_MF.Checked) Opt |= IpV4FragmentationOptions.MoreFragments;
            if (Opt != 0 || off != 0)
            {
                ipV4Layer.Fragmentation = new IpV4Fragmentation(Opt, off);
            }
                        
            return ipV4Layer;
        }

        private Packet BuildIPPacket()
        {
            EthernetLayer ethernetLayer = BuildEthernetLayer();
            ILayer ipLayer = BuildIPLayer();

            PayloadLayer payloadLayer =
                new PayloadLayer
                {
                    Data = new Datagram(Encoding.ASCII.GetBytes(textBox_IP_Payload.Text)),
                };

            PacketBuilder builder = new PacketBuilder(ethernetLayer, ipLayer, payloadLayer);

            Packet pkt = builder.Build(DateTime.Now);

            if (comboBox_IP_Version.SelectedIndex == 1)
            {
                byte b = pkt.Buffer[14];
                b = (byte)(b & 0x0f);
                b = (byte)(b | (6 << 4));
                pkt.Buffer[14] = b;
            }

            return pkt;
        }

        private TcpLayer BuildTCPLayer()
        {
            TcpLayer tcpLayer =
                new TcpLayer
                {
                    SourcePort = UInt16.Parse(textBox_TCP_Src.Text),
                    DestinationPort = UInt16.Parse(textBox_TCP_Dst.Text),
                    Checksum = null, // Will be filled automatically.
                    SequenceNumber = UInt32.Parse(textBox_TCP_SeqNumber.Text),
                    AcknowledgmentNumber = UInt32.Parse(textBox_TCP_AckNumber.Text),
                    ControlBits = TcpControlBits.None,
                    Window = UInt16.Parse(textBox_TCP_WinSize.Text),
                    UrgentPointer = UInt16.Parse(textBox_TCP_Urgent.Text),
                    Options = TcpOptions.None,
                };

            if (textBox_TCP_Checksum.Text.Length > 0)
                tcpLayer.Checksum = UInt16.Parse(textBox_TCP_Checksum.Text);

            TcpControlBits flag = 0;
            if (checkBox_TCP_Ack.Checked) flag |= TcpControlBits.Acknowledgment;
            if (checkBox_TCP_CWR.Checked) flag |= TcpControlBits.CongestionWindowReduced;
            if (checkBox_TCP_ECNEcho.Checked) flag |= TcpControlBits.ExplicitCongestionNotificationEcho;
            if (checkBox_TCP_Fin.Checked) flag |= TcpControlBits.Fin;
            if (checkBox_TCP_Nonce.Checked) flag |= TcpControlBits.NonceSum;
            if (checkBox_TCP_Push.Checked) flag |= TcpControlBits.Push;
            if (checkBox_TCP_Reset.Checked) flag |= TcpControlBits.Reset;
            if (checkBox_TCP_Syn.Checked) flag |= TcpControlBits.Synchronize;
            if (checkBox_TCP_Urgent.Checked) flag |= TcpControlBits.Urgent;

            tcpLayer.ControlBits = flag;

            return tcpLayer;
        }

        private Packet BuildTCPPacket()
        {
            EthernetLayer ethernetLayer = BuildEthernetLayer();
            ILayer ipV4Layer = BuildIPLayer();
            TcpLayer tcpLayer = BuildTCPLayer();

            PayloadLayer payloadLayer =
                new PayloadLayer
                {
                    Data = new Datagram(Encoding.ASCII.GetBytes(textBox_TCP_Payload.Text)),
                };

            PacketBuilder builder = new PacketBuilder(ethernetLayer, ipV4Layer, tcpLayer, payloadLayer);

            Packet pkt = builder.Build(DateTime.Now);

            if (comboBox_IP_Version.SelectedIndex == 1)
            {
                byte b = pkt.Buffer[14];
                b = (byte)(b & 0x0f);
                b = (byte)(b | (6 << 4));
                pkt.Buffer[14] = b;
            }

            return pkt;
        }

        private void button_TCP_Send_Click(object sender, EventArgs e)
        {
            SendPacket(BuildTCPPacket());
        }
        
        private Packet BuildUDPPacket()
        {
            EthernetLayer ethernetLayer = BuildEthernetLayer();
            ILayer ipV4Layer = BuildIPLayer();

            UdpLayer udpLayer =
                new UdpLayer
                {
                    SourcePort = UInt16.Parse(textBox_UDP_SrcPort.Text),
                    DestinationPort = UInt16.Parse(textBox_UDP_DstPort.Text),
                    Checksum = null, // Will be filled automatically.
                    CalculateChecksumValue = true,
                };

            if (textBox_UDP_Checksum.Text.Length > 0)
                udpLayer.Checksum = UInt16.Parse(textBox_UDP_Checksum.Text);

            UInt16 length = UInt16.Parse(textBox_UDP_Length.Text);
            length -= 8;

            PayloadLayer payloadLayer =
                new PayloadLayer
                {
                    Data = new Datagram(Encoding.ASCII.GetBytes(textBox_UDP_Payload.Text.Substring(0, length))),
                };

            PacketBuilder builder = new PacketBuilder(ethernetLayer, ipV4Layer, udpLayer, payloadLayer);

            Packet pkt = builder.Build(DateTime.Now);

            if (comboBox_IP_Version.SelectedIndex == 1)
            {
                byte b = pkt.Buffer[14];
                b = (byte)(b & 0x0f);
                b = (byte)(b | (6 << 4));
                pkt.Buffer[14] = b;
            }

            return pkt;
        }

        private void button_UDP_Send_Click(object sender, EventArgs e)
        {
            SendPacket(BuildUDPPacket());
        }

        private IcmpLayer BuildICMPLayer()
        {
            IcmpLayer icmpLayer;

            if (radioButton_ICMP_EchoRequest.Checked)
            {
                icmpLayer = new IcmpEchoLayer();
            }
            else
            {
                icmpLayer = new IcmpEchoReplyLayer();
            }

            if (textBox_ICMP_Checksum.Text.Length > 0)
                icmpLayer.Checksum = UInt16.Parse(textBox_ICMP_Checksum.Text);

            return icmpLayer;
        }

        private Packet BuildICMPPacket()
        {
            EthernetLayer ethernetLayer = BuildEthernetLayer();
            ILayer ipV4Layer = BuildIPLayer();
            IcmpLayer icmpLayer = BuildICMPLayer();

            PayloadLayer payloadLayer =
                new PayloadLayer
                {
                    Data = new Datagram(Encoding.ASCII.GetBytes(textBox_ICMP_Payload.Text)),
                };

            PacketBuilder builder = new PacketBuilder(ethernetLayer, ipV4Layer, icmpLayer, payloadLayer);

            Packet pkt = builder.Build(DateTime.Now);

            if (comboBox_IP_Version.SelectedIndex == 1)
            {
                byte b = pkt.Buffer[14];
                b = (byte)(b & 0x0f);
                b = (byte)(b | (6 << 4));
                pkt.Buffer[14] = b;
            }

            return pkt;
        }

        private void button_ICMP_Send_Click(object sender, EventArgs e)
        {
            SendPacket(BuildICMPPacket());
        }

        private void SavePacket(Packet pkt)
        {
            Stream myStream;
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();

            // saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            //saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = saveFileDialog1.OpenFile()) != null)
                {
                    myStream.Write(pkt.Buffer, 0, pkt.Buffer.Length);
                    // Code to write the stream goes here.
                    myStream.Close();
                }
            }
        }

        private void button_Eth_Save_Click(object sender, EventArgs e)
        {
            SavePacket(BuildEthernetPacket());
        }

        private void button_IP_Save_Click(object sender, EventArgs e)
        {
            SavePacket(BuildIPPacket());
        }

        private void button_TCP_Save_Click(object sender, EventArgs e)
        {
            SavePacket(BuildTCPPacket());
        }

        private void button_UDP_Save_Click(object sender, EventArgs e)
        {
            SavePacket(BuildUDPPacket());
        }

        private void button_ICMP_Save_Click(object sender, EventArgs e)
        {
            SavePacket(BuildICMPPacket());
        }

        private void AddToPacketList(String filename, byte[] data)
        {
            bool found = false;
            for (int i = 0; i < packet_names.Count; ++i)
            {
                if (packet_names[i] == filename)
                {
                    found = true;
                    break;
                }
            }

            if (found)
            {
                MessageBox.Show("Packet with this name is in use already.\nRename file before loading.");
            }
            else
            {
                packet_names.Add(filename);
                packet_datum.Add(data);
                listBox_Advanced_PreparedPackets.Items.Add(filename);
            }
        }

        private void button_Advanced_Load_Click(object sender, EventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            String fileName;
            byte[] data;

            // openFileDialog1.InitialDirectory = "c:\\";
            // openFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            // openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        int len = (int)myStream.Length;
                        data = new byte[len];
                        fileName = openFileDialog1.SafeFileName;
                        int res = myStream.Read(data, 0, len);
                        AddToPacketList(fileName, data);
                        myStream.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void button_Advanced_Send_Click(object sender, EventArgs e)
        {
            int idx = listBox_Advanced_PreparedPackets.SelectedIndex;
            Packet packet = new Packet(packet_datum[idx], DateTime.Now, 0);
            SendPacket(packet);
        }
    }
}


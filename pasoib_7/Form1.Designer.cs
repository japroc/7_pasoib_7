﻿namespace pasoib_7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox_Devices = new System.Windows.Forms.ComboBox();
            this.tabControl_Ethernet = new System.Windows.Forms.TabControl();
            this.tabPage_Ethernet = new System.Windows.Forms.TabPage();
            this.button_Eth_Save = new System.Windows.Forms.Button();
            this.textBox_Eth_Payload = new System.Windows.Forms.TextBox();
            this.label_Eth_Payload = new System.Windows.Forms.Label();
            this.label_Eth_Proto = new System.Windows.Forms.Label();
            this.comboBox_Eth_Proto = new System.Windows.Forms.ComboBox();
            this.button_Eth = new System.Windows.Forms.Button();
            this.textBox_Eth_Dest = new System.Windows.Forms.TextBox();
            this.textBox_Eth_Source = new System.Windows.Forms.TextBox();
            this.label_Eth_Dest = new System.Windows.Forms.Label();
            this.label_Eth_Source = new System.Windows.Forms.Label();
            this.tabPage_IP = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.label_IP_FragmentOffset = new System.Windows.Forms.Label();
            this.textBox_IP_FragmentOffset = new System.Windows.Forms.TextBox();
            this.button_IP_Save = new System.Windows.Forms.Button();
            this.textBox_IP_Payload = new System.Windows.Forms.TextBox();
            this.label_IP_Payload = new System.Windows.Forms.Label();
            this.checkBox_IP_MF = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_DF = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_Reserved = new System.Windows.Forms.CheckBox();
            this.label_IP_Flags = new System.Windows.Forms.Label();
            this.textBox_IP_Identification = new System.Windows.Forms.TextBox();
            this.textBox_IP_Checksum = new System.Windows.Forms.TextBox();
            this.comboBox_IP_Proto = new System.Windows.Forms.ComboBox();
            this.textBox_IP_TTL = new System.Windows.Forms.TextBox();
            this.label_IP_Identification = new System.Windows.Forms.Label();
            this.label_IP_Checksum = new System.Windows.Forms.Label();
            this.label_IP_Proto = new System.Windows.Forms.Label();
            this.label_IP_TTL = new System.Windows.Forms.Label();
            this.checkBox_IP_X = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_C = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_R = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_T = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_D = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_3bit = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_2bit = new System.Windows.Forms.CheckBox();
            this.checkBox_IP_1bit = new System.Windows.Forms.CheckBox();
            this.label_IP_TypeofService = new System.Windows.Forms.Label();
            this.textBox_IP_TotalLength = new System.Windows.Forms.TextBox();
            this.label_IP_TotalLength = new System.Windows.Forms.Label();
            this.button_IP_Send = new System.Windows.Forms.Button();
            this.label_IP_HeaderLength = new System.Windows.Forms.Label();
            this.textBox_IP_HeaderLength = new System.Windows.Forms.TextBox();
            this.textBox_IP_Dest = new System.Windows.Forms.TextBox();
            this.textBox_IP_Src = new System.Windows.Forms.TextBox();
            this.comboBox_IP_Version = new System.Windows.Forms.ComboBox();
            this.label_IP_Dest = new System.Windows.Forms.Label();
            this.label_Source = new System.Windows.Forms.Label();
            this.label_IP_Version = new System.Windows.Forms.Label();
            this.tabPage_TCP = new System.Windows.Forms.TabPage();
            this.button_TCP_Save = new System.Windows.Forms.Button();
            this.textBox_TCP_Payload = new System.Windows.Forms.TextBox();
            this.label_TCP_Payload = new System.Windows.Forms.Label();
            this.button_TCP_Send = new System.Windows.Forms.Button();
            this.checkBox_TCP_Fin = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Syn = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Reset = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Push = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Ack = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Urgent = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_ECNEcho = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_CWR = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Nonce = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Reserved3 = new System.Windows.Forms.CheckBox();
            this.checkBox_TCP_Reserved2 = new System.Windows.Forms.CheckBox();
            this.textBox_TCP_HeaderLength = new System.Windows.Forms.TextBox();
            this.label_TCP_HeaderLength = new System.Windows.Forms.Label();
            this.checkBox_TCP_Reserved1 = new System.Windows.Forms.CheckBox();
            this.textBox_TCP_Urgent = new System.Windows.Forms.TextBox();
            this.textBox_TCP_Checksum = new System.Windows.Forms.TextBox();
            this.textBox_TCP_WinSize = new System.Windows.Forms.TextBox();
            this.textBox_TCP_AckNumber = new System.Windows.Forms.TextBox();
            this.textBox_TCP_SeqNumber = new System.Windows.Forms.TextBox();
            this.textBox_TCP_Dst = new System.Windows.Forms.TextBox();
            this.textBox_TCP_Src = new System.Windows.Forms.TextBox();
            this.label_TCP_ = new System.Windows.Forms.Label();
            this.label_TCP_Checksum = new System.Windows.Forms.Label();
            this.label_TCP_WinSize = new System.Windows.Forms.Label();
            this.label_TCP_AckNumber = new System.Windows.Forms.Label();
            this.label_TCP_SeqNumber = new System.Windows.Forms.Label();
            this.label_TCP_Dst = new System.Windows.Forms.Label();
            this.label_TCP_Src = new System.Windows.Forms.Label();
            this.tabPage_UDP = new System.Windows.Forms.TabPage();
            this.button_UDP_Save = new System.Windows.Forms.Button();
            this.label_UDP_Payload = new System.Windows.Forms.Label();
            this.textBox_UDP_Payload = new System.Windows.Forms.TextBox();
            this.label_UDP_Checksum = new System.Windows.Forms.Label();
            this.label_UDP_Length = new System.Windows.Forms.Label();
            this.label_UDP_Dst = new System.Windows.Forms.Label();
            this.textBox_UDP_Checksum = new System.Windows.Forms.TextBox();
            this.textBox_UDP_Length = new System.Windows.Forms.TextBox();
            this.textBox_UDP_DstPort = new System.Windows.Forms.TextBox();
            this.textBox_UDP_SrcPort = new System.Windows.Forms.TextBox();
            this.label_UDP_Src = new System.Windows.Forms.Label();
            this.button_UDP_Send = new System.Windows.Forms.Button();
            this.tabPage_ICMP = new System.Windows.Forms.TabPage();
            this.button_ICMP_Save = new System.Windows.Forms.Button();
            this.label_ICMP_Chechsum = new System.Windows.Forms.Label();
            this.textBox_ICMP_Checksum = new System.Windows.Forms.TextBox();
            this.radioButton_ICMP_EchoReply = new System.Windows.Forms.RadioButton();
            this.radioButton_ICMP_EchoRequest = new System.Windows.Forms.RadioButton();
            this.button_ICMP_Send = new System.Windows.Forms.Button();
            this.tabPage_Advanced = new System.Windows.Forms.TabPage();
            this.button_Advanced_Send = new System.Windows.Forms.Button();
            this.button_Advanced_Load = new System.Windows.Forms.Button();
            this.listBox_Advanced_PreparedPackets = new System.Windows.Forms.ListBox();
            this.textBox_ICMP_Payload = new System.Windows.Forms.TextBox();
            this.label_ICMP_Payload = new System.Windows.Forms.Label();
            this.tabControl_Ethernet.SuspendLayout();
            this.tabPage_Ethernet.SuspendLayout();
            this.tabPage_IP.SuspendLayout();
            this.tabPage_TCP.SuspendLayout();
            this.tabPage_UDP.SuspendLayout();
            this.tabPage_ICMP.SuspendLayout();
            this.tabPage_Advanced.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox_Devices
            // 
            this.comboBox_Devices.FormattingEnabled = true;
            this.comboBox_Devices.Location = new System.Drawing.Point(12, 13);
            this.comboBox_Devices.Name = "comboBox_Devices";
            this.comboBox_Devices.Size = new System.Drawing.Size(444, 21);
            this.comboBox_Devices.TabIndex = 0;
            // 
            // tabControl_Ethernet
            // 
            this.tabControl_Ethernet.Controls.Add(this.tabPage_Ethernet);
            this.tabControl_Ethernet.Controls.Add(this.tabPage_IP);
            this.tabControl_Ethernet.Controls.Add(this.tabPage_TCP);
            this.tabControl_Ethernet.Controls.Add(this.tabPage_UDP);
            this.tabControl_Ethernet.Controls.Add(this.tabPage_ICMP);
            this.tabControl_Ethernet.Controls.Add(this.tabPage_Advanced);
            this.tabControl_Ethernet.Location = new System.Drawing.Point(13, 41);
            this.tabControl_Ethernet.Name = "tabControl_Ethernet";
            this.tabControl_Ethernet.SelectedIndex = 0;
            this.tabControl_Ethernet.Size = new System.Drawing.Size(447, 354);
            this.tabControl_Ethernet.TabIndex = 1;
            // 
            // tabPage_Ethernet
            // 
            this.tabPage_Ethernet.Controls.Add(this.button_Eth_Save);
            this.tabPage_Ethernet.Controls.Add(this.textBox_Eth_Payload);
            this.tabPage_Ethernet.Controls.Add(this.label_Eth_Payload);
            this.tabPage_Ethernet.Controls.Add(this.label_Eth_Proto);
            this.tabPage_Ethernet.Controls.Add(this.comboBox_Eth_Proto);
            this.tabPage_Ethernet.Controls.Add(this.button_Eth);
            this.tabPage_Ethernet.Controls.Add(this.textBox_Eth_Dest);
            this.tabPage_Ethernet.Controls.Add(this.textBox_Eth_Source);
            this.tabPage_Ethernet.Controls.Add(this.label_Eth_Dest);
            this.tabPage_Ethernet.Controls.Add(this.label_Eth_Source);
            this.tabPage_Ethernet.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Ethernet.Name = "tabPage_Ethernet";
            this.tabPage_Ethernet.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Ethernet.Size = new System.Drawing.Size(439, 328);
            this.tabPage_Ethernet.TabIndex = 0;
            this.tabPage_Ethernet.Text = "Ethernet";
            this.tabPage_Ethernet.UseVisualStyleBackColor = true;
            // 
            // button_Eth_Save
            // 
            this.button_Eth_Save.Location = new System.Drawing.Point(56, 178);
            this.button_Eth_Save.Name = "button_Eth_Save";
            this.button_Eth_Save.Size = new System.Drawing.Size(75, 23);
            this.button_Eth_Save.TabIndex = 9;
            this.button_Eth_Save.Text = "Save";
            this.button_Eth_Save.UseVisualStyleBackColor = true;
            this.button_Eth_Save.Click += new System.EventHandler(this.button_Eth_Save_Click);
            // 
            // textBox_Eth_Payload
            // 
            this.textBox_Eth_Payload.Location = new System.Drawing.Point(74, 134);
            this.textBox_Eth_Payload.Name = "textBox_Eth_Payload";
            this.textBox_Eth_Payload.Size = new System.Drawing.Size(143, 20);
            this.textBox_Eth_Payload.TabIndex = 8;
            // 
            // label_Eth_Payload
            // 
            this.label_Eth_Payload.AutoSize = true;
            this.label_Eth_Payload.Location = new System.Drawing.Point(26, 137);
            this.label_Eth_Payload.Name = "label_Eth_Payload";
            this.label_Eth_Payload.Size = new System.Drawing.Size(48, 13);
            this.label_Eth_Payload.TabIndex = 7;
            this.label_Eth_Payload.Text = "Payload:";
            // 
            // label_Eth_Proto
            // 
            this.label_Eth_Proto.AutoSize = true;
            this.label_Eth_Proto.Location = new System.Drawing.Point(26, 99);
            this.label_Eth_Proto.Name = "label_Eth_Proto";
            this.label_Eth_Proto.Size = new System.Drawing.Size(35, 13);
            this.label_Eth_Proto.TabIndex = 6;
            this.label_Eth_Proto.Text = "Proto:";
            // 
            // comboBox_Eth_Proto
            // 
            this.comboBox_Eth_Proto.FormattingEnabled = true;
            this.comboBox_Eth_Proto.Items.AddRange(new object[] {
            "IPv4",
            "IPv6",
            "Arp",
            "None"});
            this.comboBox_Eth_Proto.Location = new System.Drawing.Point(74, 96);
            this.comboBox_Eth_Proto.Name = "comboBox_Eth_Proto";
            this.comboBox_Eth_Proto.Size = new System.Drawing.Size(143, 21);
            this.comboBox_Eth_Proto.TabIndex = 5;
            // 
            // button_Eth
            // 
            this.button_Eth.Location = new System.Drawing.Point(137, 178);
            this.button_Eth.Name = "button_Eth";
            this.button_Eth.Size = new System.Drawing.Size(75, 23);
            this.button_Eth.TabIndex = 4;
            this.button_Eth.Text = "Send";
            this.button_Eth.UseVisualStyleBackColor = true;
            this.button_Eth.Click += new System.EventHandler(this.button_Eth_Click);
            // 
            // textBox_Eth_Dest
            // 
            this.textBox_Eth_Dest.Location = new System.Drawing.Point(74, 59);
            this.textBox_Eth_Dest.Name = "textBox_Eth_Dest";
            this.textBox_Eth_Dest.Size = new System.Drawing.Size(143, 20);
            this.textBox_Eth_Dest.TabIndex = 3;
            this.textBox_Eth_Dest.Text = "02:02:02:02:02:02";
            // 
            // textBox_Eth_Source
            // 
            this.textBox_Eth_Source.Location = new System.Drawing.Point(74, 24);
            this.textBox_Eth_Source.Name = "textBox_Eth_Source";
            this.textBox_Eth_Source.Size = new System.Drawing.Size(143, 20);
            this.textBox_Eth_Source.TabIndex = 2;
            this.textBox_Eth_Source.Text = "01:01:01:01:01:01";
            // 
            // label_Eth_Dest
            // 
            this.label_Eth_Dest.AutoSize = true;
            this.label_Eth_Dest.Location = new System.Drawing.Point(26, 62);
            this.label_Eth_Dest.Name = "label_Eth_Dest";
            this.label_Eth_Dest.Size = new System.Drawing.Size(32, 13);
            this.label_Eth_Dest.TabIndex = 1;
            this.label_Eth_Dest.Text = "Dest:";
            // 
            // label_Eth_Source
            // 
            this.label_Eth_Source.AutoSize = true;
            this.label_Eth_Source.Location = new System.Drawing.Point(26, 27);
            this.label_Eth_Source.Name = "label_Eth_Source";
            this.label_Eth_Source.Size = new System.Drawing.Size(44, 13);
            this.label_Eth_Source.TabIndex = 0;
            this.label_Eth_Source.Text = "Source:";
            // 
            // tabPage_IP
            // 
            this.tabPage_IP.Controls.Add(this.label1);
            this.tabPage_IP.Controls.Add(this.label_IP_FragmentOffset);
            this.tabPage_IP.Controls.Add(this.textBox_IP_FragmentOffset);
            this.tabPage_IP.Controls.Add(this.button_IP_Save);
            this.tabPage_IP.Controls.Add(this.textBox_IP_Payload);
            this.tabPage_IP.Controls.Add(this.label_IP_Payload);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_MF);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_DF);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_Reserved);
            this.tabPage_IP.Controls.Add(this.label_IP_Flags);
            this.tabPage_IP.Controls.Add(this.textBox_IP_Identification);
            this.tabPage_IP.Controls.Add(this.textBox_IP_Checksum);
            this.tabPage_IP.Controls.Add(this.comboBox_IP_Proto);
            this.tabPage_IP.Controls.Add(this.textBox_IP_TTL);
            this.tabPage_IP.Controls.Add(this.label_IP_Identification);
            this.tabPage_IP.Controls.Add(this.label_IP_Checksum);
            this.tabPage_IP.Controls.Add(this.label_IP_Proto);
            this.tabPage_IP.Controls.Add(this.label_IP_TTL);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_X);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_C);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_R);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_T);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_D);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_3bit);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_2bit);
            this.tabPage_IP.Controls.Add(this.checkBox_IP_1bit);
            this.tabPage_IP.Controls.Add(this.label_IP_TypeofService);
            this.tabPage_IP.Controls.Add(this.textBox_IP_TotalLength);
            this.tabPage_IP.Controls.Add(this.label_IP_TotalLength);
            this.tabPage_IP.Controls.Add(this.button_IP_Send);
            this.tabPage_IP.Controls.Add(this.label_IP_HeaderLength);
            this.tabPage_IP.Controls.Add(this.textBox_IP_HeaderLength);
            this.tabPage_IP.Controls.Add(this.textBox_IP_Dest);
            this.tabPage_IP.Controls.Add(this.textBox_IP_Src);
            this.tabPage_IP.Controls.Add(this.comboBox_IP_Version);
            this.tabPage_IP.Controls.Add(this.label_IP_Dest);
            this.tabPage_IP.Controls.Add(this.label_Source);
            this.tabPage_IP.Controls.Add(this.label_IP_Version);
            this.tabPage_IP.Location = new System.Drawing.Point(4, 22);
            this.tabPage_IP.Name = "tabPage_IP";
            this.tabPage_IP.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_IP.Size = new System.Drawing.Size(439, 328);
            this.tabPage_IP.TabIndex = 1;
            this.tabPage_IP.Text = "IP";
            this.tabPage_IP.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(311, 148);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 37;
            this.label1.Text = "(must be devided by 8)";
            // 
            // label_IP_FragmentOffset
            // 
            this.label_IP_FragmentOffset.AutoSize = true;
            this.label_IP_FragmentOffset.Location = new System.Drawing.Point(238, 128);
            this.label_IP_FragmentOffset.Name = "label_IP_FragmentOffset";
            this.label_IP_FragmentOffset.Size = new System.Drawing.Size(85, 13);
            this.label_IP_FragmentOffset.TabIndex = 36;
            this.label_IP_FragmentOffset.Text = "Fragment Offset:";
            // 
            // textBox_IP_FragmentOffset
            // 
            this.textBox_IP_FragmentOffset.Location = new System.Drawing.Point(329, 125);
            this.textBox_IP_FragmentOffset.Name = "textBox_IP_FragmentOffset";
            this.textBox_IP_FragmentOffset.Size = new System.Drawing.Size(82, 20);
            this.textBox_IP_FragmentOffset.TabIndex = 35;
            // 
            // button_IP_Save
            // 
            this.button_IP_Save.Location = new System.Drawing.Point(358, 266);
            this.button_IP_Save.Name = "button_IP_Save";
            this.button_IP_Save.Size = new System.Drawing.Size(75, 23);
            this.button_IP_Save.TabIndex = 34;
            this.button_IP_Save.Text = "Save";
            this.button_IP_Save.UseVisualStyleBackColor = true;
            this.button_IP_Save.Click += new System.EventHandler(this.button_IP_Save_Click);
            // 
            // textBox_IP_Payload
            // 
            this.textBox_IP_Payload.Location = new System.Drawing.Point(79, 297);
            this.textBox_IP_Payload.Name = "textBox_IP_Payload";
            this.textBox_IP_Payload.Size = new System.Drawing.Size(246, 20);
            this.textBox_IP_Payload.TabIndex = 33;
            // 
            // label_IP_Payload
            // 
            this.label_IP_Payload.AutoSize = true;
            this.label_IP_Payload.Location = new System.Drawing.Point(25, 300);
            this.label_IP_Payload.Name = "label_IP_Payload";
            this.label_IP_Payload.Size = new System.Drawing.Size(48, 13);
            this.label_IP_Payload.TabIndex = 32;
            this.label_IP_Payload.Text = "Payload:";
            // 
            // checkBox_IP_MF
            // 
            this.checkBox_IP_MF.AutoSize = true;
            this.checkBox_IP_MF.Location = new System.Drawing.Point(230, 246);
            this.checkBox_IP_MF.Name = "checkBox_IP_MF";
            this.checkBox_IP_MF.Size = new System.Drawing.Size(41, 17);
            this.checkBox_IP_MF.TabIndex = 31;
            this.checkBox_IP_MF.Text = "MF";
            this.checkBox_IP_MF.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_DF
            // 
            this.checkBox_IP_DF.AutoSize = true;
            this.checkBox_IP_DF.Location = new System.Drawing.Point(230, 222);
            this.checkBox_IP_DF.Name = "checkBox_IP_DF";
            this.checkBox_IP_DF.Size = new System.Drawing.Size(40, 17);
            this.checkBox_IP_DF.TabIndex = 30;
            this.checkBox_IP_DF.Text = "DF";
            this.checkBox_IP_DF.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_Reserved
            // 
            this.checkBox_IP_Reserved.AutoSize = true;
            this.checkBox_IP_Reserved.Enabled = false;
            this.checkBox_IP_Reserved.Location = new System.Drawing.Point(230, 198);
            this.checkBox_IP_Reserved.Name = "checkBox_IP_Reserved";
            this.checkBox_IP_Reserved.Size = new System.Drawing.Size(72, 17);
            this.checkBox_IP_Reserved.TabIndex = 29;
            this.checkBox_IP_Reserved.Text = "Reserved";
            this.checkBox_IP_Reserved.UseVisualStyleBackColor = true;
            // 
            // label_IP_Flags
            // 
            this.label_IP_Flags.AutoSize = true;
            this.label_IP_Flags.Location = new System.Drawing.Point(255, 182);
            this.label_IP_Flags.Name = "label_IP_Flags";
            this.label_IP_Flags.Size = new System.Drawing.Size(32, 13);
            this.label_IP_Flags.TabIndex = 28;
            this.label_IP_Flags.Text = "Flags";
            // 
            // textBox_IP_Identification
            // 
            this.textBox_IP_Identification.Location = new System.Drawing.Point(314, 99);
            this.textBox_IP_Identification.Name = "textBox_IP_Identification";
            this.textBox_IP_Identification.Size = new System.Drawing.Size(97, 20);
            this.textBox_IP_Identification.TabIndex = 27;
            this.textBox_IP_Identification.Text = "123";
            // 
            // textBox_IP_Checksum
            // 
            this.textBox_IP_Checksum.Location = new System.Drawing.Point(304, 72);
            this.textBox_IP_Checksum.Name = "textBox_IP_Checksum";
            this.textBox_IP_Checksum.Size = new System.Drawing.Size(107, 20);
            this.textBox_IP_Checksum.TabIndex = 26;
            // 
            // comboBox_IP_Proto
            // 
            this.comboBox_IP_Proto.FormattingEnabled = true;
            this.comboBox_IP_Proto.Items.AddRange(new object[] {
            "TCP",
            "UDP",
            "ICMP"});
            this.comboBox_IP_Proto.Location = new System.Drawing.Point(293, 44);
            this.comboBox_IP_Proto.Name = "comboBox_IP_Proto";
            this.comboBox_IP_Proto.Size = new System.Drawing.Size(118, 21);
            this.comboBox_IP_Proto.TabIndex = 25;
            // 
            // textBox_IP_TTL
            // 
            this.textBox_IP_TTL.Location = new System.Drawing.Point(274, 18);
            this.textBox_IP_TTL.Name = "textBox_IP_TTL";
            this.textBox_IP_TTL.Size = new System.Drawing.Size(137, 20);
            this.textBox_IP_TTL.TabIndex = 24;
            this.textBox_IP_TTL.Text = "64";
            // 
            // label_IP_Identification
            // 
            this.label_IP_Identification.AutoSize = true;
            this.label_IP_Identification.Location = new System.Drawing.Point(238, 102);
            this.label_IP_Identification.Name = "label_IP_Identification";
            this.label_IP_Identification.Size = new System.Drawing.Size(70, 13);
            this.label_IP_Identification.TabIndex = 23;
            this.label_IP_Identification.Text = "Identification:";
            // 
            // label_IP_Checksum
            // 
            this.label_IP_Checksum.AutoSize = true;
            this.label_IP_Checksum.Location = new System.Drawing.Point(238, 75);
            this.label_IP_Checksum.Name = "label_IP_Checksum";
            this.label_IP_Checksum.Size = new System.Drawing.Size(60, 13);
            this.label_IP_Checksum.TabIndex = 22;
            this.label_IP_Checksum.Text = "Checksum:";
            // 
            // label_IP_Proto
            // 
            this.label_IP_Proto.AutoSize = true;
            this.label_IP_Proto.Location = new System.Drawing.Point(238, 49);
            this.label_IP_Proto.Name = "label_IP_Proto";
            this.label_IP_Proto.Size = new System.Drawing.Size(49, 13);
            this.label_IP_Proto.TabIndex = 21;
            this.label_IP_Proto.Text = "Protocol:";
            // 
            // label_IP_TTL
            // 
            this.label_IP_TTL.AutoSize = true;
            this.label_IP_TTL.Location = new System.Drawing.Point(238, 21);
            this.label_IP_TTL.Name = "label_IP_TTL";
            this.label_IP_TTL.Size = new System.Drawing.Size(30, 13);
            this.label_IP_TTL.TabIndex = 20;
            this.label_IP_TTL.Text = "TTL:";
            // 
            // checkBox_IP_X
            // 
            this.checkBox_IP_X.AutoSize = true;
            this.checkBox_IP_X.Location = new System.Drawing.Point(119, 274);
            this.checkBox_IP_X.Name = "checkBox_IP_X";
            this.checkBox_IP_X.Size = new System.Drawing.Size(33, 17);
            this.checkBox_IP_X.TabIndex = 19;
            this.checkBox_IP_X.Text = "X";
            this.checkBox_IP_X.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_C
            // 
            this.checkBox_IP_C.AutoSize = true;
            this.checkBox_IP_C.Location = new System.Drawing.Point(119, 249);
            this.checkBox_IP_C.Name = "checkBox_IP_C";
            this.checkBox_IP_C.Size = new System.Drawing.Size(33, 17);
            this.checkBox_IP_C.TabIndex = 18;
            this.checkBox_IP_C.Text = "C";
            this.checkBox_IP_C.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_R
            // 
            this.checkBox_IP_R.AutoSize = true;
            this.checkBox_IP_R.Location = new System.Drawing.Point(119, 226);
            this.checkBox_IP_R.Name = "checkBox_IP_R";
            this.checkBox_IP_R.Size = new System.Drawing.Size(34, 17);
            this.checkBox_IP_R.TabIndex = 17;
            this.checkBox_IP_R.Text = "R";
            this.checkBox_IP_R.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_T
            // 
            this.checkBox_IP_T.AutoSize = true;
            this.checkBox_IP_T.Location = new System.Drawing.Point(119, 202);
            this.checkBox_IP_T.Name = "checkBox_IP_T";
            this.checkBox_IP_T.Size = new System.Drawing.Size(33, 17);
            this.checkBox_IP_T.TabIndex = 16;
            this.checkBox_IP_T.Text = "T";
            this.checkBox_IP_T.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_D
            // 
            this.checkBox_IP_D.AutoSize = true;
            this.checkBox_IP_D.Location = new System.Drawing.Point(119, 178);
            this.checkBox_IP_D.Name = "checkBox_IP_D";
            this.checkBox_IP_D.Size = new System.Drawing.Size(34, 17);
            this.checkBox_IP_D.TabIndex = 15;
            this.checkBox_IP_D.Text = "D";
            this.checkBox_IP_D.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_3bit
            // 
            this.checkBox_IP_3bit.AutoSize = true;
            this.checkBox_IP_3bit.Location = new System.Drawing.Point(66, 226);
            this.checkBox_IP_3bit.Name = "checkBox_IP_3bit";
            this.checkBox_IP_3bit.Size = new System.Drawing.Size(46, 17);
            this.checkBox_IP_3bit.TabIndex = 14;
            this.checkBox_IP_3bit.Text = "3 bit";
            this.checkBox_IP_3bit.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_2bit
            // 
            this.checkBox_IP_2bit.AutoSize = true;
            this.checkBox_IP_2bit.Location = new System.Drawing.Point(66, 202);
            this.checkBox_IP_2bit.Name = "checkBox_IP_2bit";
            this.checkBox_IP_2bit.Size = new System.Drawing.Size(46, 17);
            this.checkBox_IP_2bit.TabIndex = 13;
            this.checkBox_IP_2bit.Text = "2 bit";
            this.checkBox_IP_2bit.UseVisualStyleBackColor = true;
            // 
            // checkBox_IP_1bit
            // 
            this.checkBox_IP_1bit.AutoSize = true;
            this.checkBox_IP_1bit.Location = new System.Drawing.Point(66, 178);
            this.checkBox_IP_1bit.Name = "checkBox_IP_1bit";
            this.checkBox_IP_1bit.Size = new System.Drawing.Size(46, 17);
            this.checkBox_IP_1bit.TabIndex = 12;
            this.checkBox_IP_1bit.Text = "1 bit";
            this.checkBox_IP_1bit.UseVisualStyleBackColor = true;
            // 
            // label_IP_TypeofService
            // 
            this.label_IP_TypeofService.AutoSize = true;
            this.label_IP_TypeofService.Location = new System.Drawing.Point(71, 162);
            this.label_IP_TypeofService.Name = "label_IP_TypeofService";
            this.label_IP_TypeofService.Size = new System.Drawing.Size(82, 13);
            this.label_IP_TypeofService.TabIndex = 11;
            this.label_IP_TypeofService.Text = "Type of Service";
            // 
            // textBox_IP_TotalLength
            // 
            this.textBox_IP_TotalLength.Location = new System.Drawing.Point(105, 126);
            this.textBox_IP_TotalLength.Name = "textBox_IP_TotalLength";
            this.textBox_IP_TotalLength.Size = new System.Drawing.Size(90, 20);
            this.textBox_IP_TotalLength.TabIndex = 10;
            this.textBox_IP_TotalLength.Text = "30";
            // 
            // label_IP_TotalLength
            // 
            this.label_IP_TotalLength.AutoSize = true;
            this.label_IP_TotalLength.Location = new System.Drawing.Point(22, 129);
            this.label_IP_TotalLength.Name = "label_IP_TotalLength";
            this.label_IP_TotalLength.Size = new System.Drawing.Size(70, 13);
            this.label_IP_TotalLength.TabIndex = 9;
            this.label_IP_TotalLength.Text = "Total Length:";
            // 
            // button_IP_Send
            // 
            this.button_IP_Send.Location = new System.Drawing.Point(358, 295);
            this.button_IP_Send.Name = "button_IP_Send";
            this.button_IP_Send.Size = new System.Drawing.Size(75, 23);
            this.button_IP_Send.TabIndex = 8;
            this.button_IP_Send.Text = "Send";
            this.button_IP_Send.UseVisualStyleBackColor = true;
            this.button_IP_Send.Click += new System.EventHandler(this.button_IP_Send_Click);
            // 
            // label_IP_HeaderLength
            // 
            this.label_IP_HeaderLength.AutoSize = true;
            this.label_IP_HeaderLength.Location = new System.Drawing.Point(22, 102);
            this.label_IP_HeaderLength.Name = "label_IP_HeaderLength";
            this.label_IP_HeaderLength.Size = new System.Drawing.Size(81, 13);
            this.label_IP_HeaderLength.TabIndex = 7;
            this.label_IP_HeaderLength.Text = "Header Length:";
            // 
            // textBox_IP_HeaderLength
            // 
            this.textBox_IP_HeaderLength.Location = new System.Drawing.Point(105, 99);
            this.textBox_IP_HeaderLength.Name = "textBox_IP_HeaderLength";
            this.textBox_IP_HeaderLength.Size = new System.Drawing.Size(90, 20);
            this.textBox_IP_HeaderLength.TabIndex = 6;
            this.textBox_IP_HeaderLength.Text = "5";
            // 
            // textBox_IP_Dest
            // 
            this.textBox_IP_Dest.Location = new System.Drawing.Point(74, 73);
            this.textBox_IP_Dest.Name = "textBox_IP_Dest";
            this.textBox_IP_Dest.Size = new System.Drawing.Size(121, 20);
            this.textBox_IP_Dest.TabIndex = 5;
            this.textBox_IP_Dest.Text = "2.2.2.2";
            // 
            // textBox_IP_Src
            // 
            this.textBox_IP_Src.Location = new System.Drawing.Point(74, 47);
            this.textBox_IP_Src.Name = "textBox_IP_Src";
            this.textBox_IP_Src.Size = new System.Drawing.Size(121, 20);
            this.textBox_IP_Src.TabIndex = 4;
            this.textBox_IP_Src.Text = "1.1.1.1";
            // 
            // comboBox_IP_Version
            // 
            this.comboBox_IP_Version.AutoCompleteCustomSource.AddRange(new string[] {
            "IPv4",
            "IPv6",
            "Auto"});
            this.comboBox_IP_Version.FormattingEnabled = true;
            this.comboBox_IP_Version.Items.AddRange(new object[] {
            "IPv4",
            "IPv6"});
            this.comboBox_IP_Version.Location = new System.Drawing.Point(74, 19);
            this.comboBox_IP_Version.Name = "comboBox_IP_Version";
            this.comboBox_IP_Version.Size = new System.Drawing.Size(121, 21);
            this.comboBox_IP_Version.TabIndex = 3;
            // 
            // label_IP_Dest
            // 
            this.label_IP_Dest.AutoSize = true;
            this.label_IP_Dest.Location = new System.Drawing.Point(22, 76);
            this.label_IP_Dest.Name = "label_IP_Dest";
            this.label_IP_Dest.Size = new System.Drawing.Size(32, 13);
            this.label_IP_Dest.TabIndex = 2;
            this.label_IP_Dest.Text = "Dest:";
            // 
            // label_Source
            // 
            this.label_Source.AutoSize = true;
            this.label_Source.Location = new System.Drawing.Point(22, 46);
            this.label_Source.Name = "label_Source";
            this.label_Source.Size = new System.Drawing.Size(44, 13);
            this.label_Source.TabIndex = 1;
            this.label_Source.Text = "Source:";
            // 
            // label_IP_Version
            // 
            this.label_IP_Version.AutoSize = true;
            this.label_IP_Version.Location = new System.Drawing.Point(22, 19);
            this.label_IP_Version.Name = "label_IP_Version";
            this.label_IP_Version.Size = new System.Drawing.Size(45, 13);
            this.label_IP_Version.TabIndex = 0;
            this.label_IP_Version.Text = "Version:";
            // 
            // tabPage_TCP
            // 
            this.tabPage_TCP.Controls.Add(this.button_TCP_Save);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_Payload);
            this.tabPage_TCP.Controls.Add(this.label_TCP_Payload);
            this.tabPage_TCP.Controls.Add(this.button_TCP_Send);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Fin);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Syn);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Reset);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Push);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Ack);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Urgent);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_ECNEcho);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_CWR);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Nonce);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Reserved3);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Reserved2);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_HeaderLength);
            this.tabPage_TCP.Controls.Add(this.label_TCP_HeaderLength);
            this.tabPage_TCP.Controls.Add(this.checkBox_TCP_Reserved1);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_Urgent);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_Checksum);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_WinSize);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_AckNumber);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_SeqNumber);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_Dst);
            this.tabPage_TCP.Controls.Add(this.textBox_TCP_Src);
            this.tabPage_TCP.Controls.Add(this.label_TCP_);
            this.tabPage_TCP.Controls.Add(this.label_TCP_Checksum);
            this.tabPage_TCP.Controls.Add(this.label_TCP_WinSize);
            this.tabPage_TCP.Controls.Add(this.label_TCP_AckNumber);
            this.tabPage_TCP.Controls.Add(this.label_TCP_SeqNumber);
            this.tabPage_TCP.Controls.Add(this.label_TCP_Dst);
            this.tabPage_TCP.Controls.Add(this.label_TCP_Src);
            this.tabPage_TCP.Location = new System.Drawing.Point(4, 22);
            this.tabPage_TCP.Name = "tabPage_TCP";
            this.tabPage_TCP.Size = new System.Drawing.Size(439, 328);
            this.tabPage_TCP.TabIndex = 2;
            this.tabPage_TCP.Text = "TCP";
            this.tabPage_TCP.UseVisualStyleBackColor = true;
            // 
            // button_TCP_Save
            // 
            this.button_TCP_Save.Location = new System.Drawing.Point(280, 302);
            this.button_TCP_Save.Name = "button_TCP_Save";
            this.button_TCP_Save.Size = new System.Drawing.Size(75, 23);
            this.button_TCP_Save.TabIndex = 33;
            this.button_TCP_Save.Text = "Save";
            this.button_TCP_Save.UseVisualStyleBackColor = true;
            this.button_TCP_Save.Click += new System.EventHandler(this.button_TCP_Save_Click);
            // 
            // textBox_TCP_Payload
            // 
            this.textBox_TCP_Payload.Location = new System.Drawing.Point(83, 254);
            this.textBox_TCP_Payload.Name = "textBox_TCP_Payload";
            this.textBox_TCP_Payload.Size = new System.Drawing.Size(279, 20);
            this.textBox_TCP_Payload.TabIndex = 32;
            // 
            // label_TCP_Payload
            // 
            this.label_TCP_Payload.AutoSize = true;
            this.label_TCP_Payload.Location = new System.Drawing.Point(25, 257);
            this.label_TCP_Payload.Name = "label_TCP_Payload";
            this.label_TCP_Payload.Size = new System.Drawing.Size(48, 13);
            this.label_TCP_Payload.TabIndex = 31;
            this.label_TCP_Payload.Text = "Payload:";
            // 
            // button_TCP_Send
            // 
            this.button_TCP_Send.Location = new System.Drawing.Point(361, 302);
            this.button_TCP_Send.Name = "button_TCP_Send";
            this.button_TCP_Send.Size = new System.Drawing.Size(75, 23);
            this.button_TCP_Send.TabIndex = 30;
            this.button_TCP_Send.Text = "Send";
            this.button_TCP_Send.UseVisualStyleBackColor = true;
            this.button_TCP_Send.Click += new System.EventHandler(this.button_TCP_Send_Click);
            // 
            // checkBox_TCP_Fin
            // 
            this.checkBox_TCP_Fin.AutoSize = true;
            this.checkBox_TCP_Fin.Location = new System.Drawing.Point(322, 166);
            this.checkBox_TCP_Fin.Name = "checkBox_TCP_Fin";
            this.checkBox_TCP_Fin.Size = new System.Drawing.Size(40, 17);
            this.checkBox_TCP_Fin.TabIndex = 29;
            this.checkBox_TCP_Fin.Text = "Fin";
            this.checkBox_TCP_Fin.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_Syn
            // 
            this.checkBox_TCP_Syn.AutoSize = true;
            this.checkBox_TCP_Syn.Location = new System.Drawing.Point(322, 142);
            this.checkBox_TCP_Syn.Name = "checkBox_TCP_Syn";
            this.checkBox_TCP_Syn.Size = new System.Drawing.Size(44, 17);
            this.checkBox_TCP_Syn.TabIndex = 28;
            this.checkBox_TCP_Syn.Text = "Syn";
            this.checkBox_TCP_Syn.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_Reset
            // 
            this.checkBox_TCP_Reset.AutoSize = true;
            this.checkBox_TCP_Reset.Location = new System.Drawing.Point(322, 118);
            this.checkBox_TCP_Reset.Name = "checkBox_TCP_Reset";
            this.checkBox_TCP_Reset.Size = new System.Drawing.Size(54, 17);
            this.checkBox_TCP_Reset.TabIndex = 27;
            this.checkBox_TCP_Reset.Text = "Reset";
            this.checkBox_TCP_Reset.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_Push
            // 
            this.checkBox_TCP_Push.AutoSize = true;
            this.checkBox_TCP_Push.Location = new System.Drawing.Point(322, 94);
            this.checkBox_TCP_Push.Name = "checkBox_TCP_Push";
            this.checkBox_TCP_Push.Size = new System.Drawing.Size(50, 17);
            this.checkBox_TCP_Push.TabIndex = 26;
            this.checkBox_TCP_Push.Text = "Push";
            this.checkBox_TCP_Push.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_Ack
            // 
            this.checkBox_TCP_Ack.AutoSize = true;
            this.checkBox_TCP_Ack.Location = new System.Drawing.Point(322, 70);
            this.checkBox_TCP_Ack.Name = "checkBox_TCP_Ack";
            this.checkBox_TCP_Ack.Size = new System.Drawing.Size(45, 17);
            this.checkBox_TCP_Ack.TabIndex = 25;
            this.checkBox_TCP_Ack.Text = "Ack";
            this.checkBox_TCP_Ack.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_Urgent
            // 
            this.checkBox_TCP_Urgent.AutoSize = true;
            this.checkBox_TCP_Urgent.Location = new System.Drawing.Point(322, 47);
            this.checkBox_TCP_Urgent.Name = "checkBox_TCP_Urgent";
            this.checkBox_TCP_Urgent.Size = new System.Drawing.Size(58, 17);
            this.checkBox_TCP_Urgent.TabIndex = 24;
            this.checkBox_TCP_Urgent.Text = "Urgent";
            this.checkBox_TCP_Urgent.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_ECNEcho
            // 
            this.checkBox_TCP_ECNEcho.AutoSize = true;
            this.checkBox_TCP_ECNEcho.Location = new System.Drawing.Point(235, 166);
            this.checkBox_TCP_ECNEcho.Name = "checkBox_TCP_ECNEcho";
            this.checkBox_TCP_ECNEcho.Size = new System.Drawing.Size(76, 17);
            this.checkBox_TCP_ECNEcho.TabIndex = 23;
            this.checkBox_TCP_ECNEcho.Text = "ECN-Echo";
            this.checkBox_TCP_ECNEcho.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_CWR
            // 
            this.checkBox_TCP_CWR.AutoSize = true;
            this.checkBox_TCP_CWR.Location = new System.Drawing.Point(235, 142);
            this.checkBox_TCP_CWR.Name = "checkBox_TCP_CWR";
            this.checkBox_TCP_CWR.Size = new System.Drawing.Size(52, 17);
            this.checkBox_TCP_CWR.TabIndex = 22;
            this.checkBox_TCP_CWR.Text = "CWR";
            this.checkBox_TCP_CWR.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_Nonce
            // 
            this.checkBox_TCP_Nonce.AutoSize = true;
            this.checkBox_TCP_Nonce.Location = new System.Drawing.Point(235, 118);
            this.checkBox_TCP_Nonce.Name = "checkBox_TCP_Nonce";
            this.checkBox_TCP_Nonce.Size = new System.Drawing.Size(58, 17);
            this.checkBox_TCP_Nonce.TabIndex = 21;
            this.checkBox_TCP_Nonce.Text = "Nonce";
            this.checkBox_TCP_Nonce.UseVisualStyleBackColor = true;
            // 
            // checkBox_TCP_Reserved3
            // 
            this.checkBox_TCP_Reserved3.AutoSize = true;
            this.checkBox_TCP_Reserved3.Location = new System.Drawing.Point(235, 94);
            this.checkBox_TCP_Reserved3.Name = "checkBox_TCP_Reserved3";
            this.checkBox_TCP_Reserved3.Size = new System.Drawing.Size(78, 17);
            this.checkBox_TCP_Reserved3.TabIndex = 20;
            this.checkBox_TCP_Reserved3.Text = "Reserved3";
            this.checkBox_TCP_Reserved3.UseVisualStyleBackColor = true;
            this.checkBox_TCP_Reserved3.Visible = false;
            // 
            // checkBox_TCP_Reserved2
            // 
            this.checkBox_TCP_Reserved2.AutoSize = true;
            this.checkBox_TCP_Reserved2.Location = new System.Drawing.Point(235, 70);
            this.checkBox_TCP_Reserved2.Name = "checkBox_TCP_Reserved2";
            this.checkBox_TCP_Reserved2.Size = new System.Drawing.Size(78, 17);
            this.checkBox_TCP_Reserved2.TabIndex = 19;
            this.checkBox_TCP_Reserved2.Text = "Reserved2";
            this.checkBox_TCP_Reserved2.UseVisualStyleBackColor = true;
            this.checkBox_TCP_Reserved2.Visible = false;
            // 
            // textBox_TCP_HeaderLength
            // 
            this.textBox_TCP_HeaderLength.Location = new System.Drawing.Point(112, 205);
            this.textBox_TCP_HeaderLength.Name = "textBox_TCP_HeaderLength";
            this.textBox_TCP_HeaderLength.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_HeaderLength.TabIndex = 18;
            // 
            // label_TCP_HeaderLength
            // 
            this.label_TCP_HeaderLength.AutoSize = true;
            this.label_TCP_HeaderLength.Location = new System.Drawing.Point(25, 208);
            this.label_TCP_HeaderLength.Name = "label_TCP_HeaderLength";
            this.label_TCP_HeaderLength.Size = new System.Drawing.Size(81, 13);
            this.label_TCP_HeaderLength.TabIndex = 17;
            this.label_TCP_HeaderLength.Text = "Header Length:";
            // 
            // checkBox_TCP_Reserved1
            // 
            this.checkBox_TCP_Reserved1.AutoSize = true;
            this.checkBox_TCP_Reserved1.Location = new System.Drawing.Point(235, 47);
            this.checkBox_TCP_Reserved1.Name = "checkBox_TCP_Reserved1";
            this.checkBox_TCP_Reserved1.Size = new System.Drawing.Size(78, 17);
            this.checkBox_TCP_Reserved1.TabIndex = 16;
            this.checkBox_TCP_Reserved1.Text = "Reserved1";
            this.checkBox_TCP_Reserved1.UseVisualStyleBackColor = true;
            this.checkBox_TCP_Reserved1.Visible = false;
            // 
            // textBox_TCP_Urgent
            // 
            this.textBox_TCP_Urgent.Location = new System.Drawing.Point(112, 178);
            this.textBox_TCP_Urgent.Name = "textBox_TCP_Urgent";
            this.textBox_TCP_Urgent.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_Urgent.TabIndex = 15;
            this.textBox_TCP_Urgent.Text = "0";
            // 
            // textBox_TCP_Checksum
            // 
            this.textBox_TCP_Checksum.Location = new System.Drawing.Point(112, 151);
            this.textBox_TCP_Checksum.Name = "textBox_TCP_Checksum";
            this.textBox_TCP_Checksum.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_Checksum.TabIndex = 14;
            // 
            // textBox_TCP_WinSize
            // 
            this.textBox_TCP_WinSize.Location = new System.Drawing.Point(112, 125);
            this.textBox_TCP_WinSize.Name = "textBox_TCP_WinSize";
            this.textBox_TCP_WinSize.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_WinSize.TabIndex = 13;
            this.textBox_TCP_WinSize.Text = "1024";
            // 
            // textBox_TCP_AckNumber
            // 
            this.textBox_TCP_AckNumber.Location = new System.Drawing.Point(112, 99);
            this.textBox_TCP_AckNumber.Name = "textBox_TCP_AckNumber";
            this.textBox_TCP_AckNumber.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_AckNumber.TabIndex = 11;
            this.textBox_TCP_AckNumber.Text = "100";
            // 
            // textBox_TCP_SeqNumber
            // 
            this.textBox_TCP_SeqNumber.Location = new System.Drawing.Point(112, 72);
            this.textBox_TCP_SeqNumber.Name = "textBox_TCP_SeqNumber";
            this.textBox_TCP_SeqNumber.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_SeqNumber.TabIndex = 10;
            this.textBox_TCP_SeqNumber.Text = "100";
            // 
            // textBox_TCP_Dst
            // 
            this.textBox_TCP_Dst.Location = new System.Drawing.Point(112, 45);
            this.textBox_TCP_Dst.Name = "textBox_TCP_Dst";
            this.textBox_TCP_Dst.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_Dst.TabIndex = 9;
            this.textBox_TCP_Dst.Text = "20";
            // 
            // textBox_TCP_Src
            // 
            this.textBox_TCP_Src.Location = new System.Drawing.Point(112, 18);
            this.textBox_TCP_Src.Name = "textBox_TCP_Src";
            this.textBox_TCP_Src.Size = new System.Drawing.Size(100, 20);
            this.textBox_TCP_Src.TabIndex = 8;
            this.textBox_TCP_Src.Text = "10";
            // 
            // label_TCP_
            // 
            this.label_TCP_.AutoSize = true;
            this.label_TCP_.Location = new System.Drawing.Point(25, 182);
            this.label_TCP_.Name = "label_TCP_";
            this.label_TCP_.Size = new System.Drawing.Size(42, 13);
            this.label_TCP_.TabIndex = 7;
            this.label_TCP_.Text = "Urgent:";
            // 
            // label_TCP_Checksum
            // 
            this.label_TCP_Checksum.AutoSize = true;
            this.label_TCP_Checksum.Location = new System.Drawing.Point(25, 155);
            this.label_TCP_Checksum.Name = "label_TCP_Checksum";
            this.label_TCP_Checksum.Size = new System.Drawing.Size(60, 13);
            this.label_TCP_Checksum.TabIndex = 6;
            this.label_TCP_Checksum.Text = "Checksum:";
            // 
            // label_TCP_WinSize
            // 
            this.label_TCP_WinSize.AutoSize = true;
            this.label_TCP_WinSize.Location = new System.Drawing.Point(25, 128);
            this.label_TCP_WinSize.Name = "label_TCP_WinSize";
            this.label_TCP_WinSize.Size = new System.Drawing.Size(72, 13);
            this.label_TCP_WinSize.TabIndex = 5;
            this.label_TCP_WinSize.Text = "Window Size:";
            // 
            // label_TCP_AckNumber
            // 
            this.label_TCP_AckNumber.AutoSize = true;
            this.label_TCP_AckNumber.Location = new System.Drawing.Point(25, 102);
            this.label_TCP_AckNumber.Name = "label_TCP_AckNumber";
            this.label_TCP_AckNumber.Size = new System.Drawing.Size(69, 13);
            this.label_TCP_AckNumber.TabIndex = 3;
            this.label_TCP_AckNumber.Text = "Ack Number:";
            // 
            // label_TCP_SeqNumber
            // 
            this.label_TCP_SeqNumber.AutoSize = true;
            this.label_TCP_SeqNumber.Location = new System.Drawing.Point(25, 75);
            this.label_TCP_SeqNumber.Name = "label_TCP_SeqNumber";
            this.label_TCP_SeqNumber.Size = new System.Drawing.Size(69, 13);
            this.label_TCP_SeqNumber.TabIndex = 2;
            this.label_TCP_SeqNumber.Text = "Seq Number:";
            // 
            // label_TCP_Dst
            // 
            this.label_TCP_Dst.AutoSize = true;
            this.label_TCP_Dst.Location = new System.Drawing.Point(25, 48);
            this.label_TCP_Dst.Name = "label_TCP_Dst";
            this.label_TCP_Dst.Size = new System.Drawing.Size(48, 13);
            this.label_TCP_Dst.TabIndex = 1;
            this.label_TCP_Dst.Text = "Dst Port:";
            // 
            // label_TCP_Src
            // 
            this.label_TCP_Src.AutoSize = true;
            this.label_TCP_Src.Location = new System.Drawing.Point(25, 21);
            this.label_TCP_Src.Name = "label_TCP_Src";
            this.label_TCP_Src.Size = new System.Drawing.Size(48, 13);
            this.label_TCP_Src.TabIndex = 0;
            this.label_TCP_Src.Text = "Src Port:";
            // 
            // tabPage_UDP
            // 
            this.tabPage_UDP.Controls.Add(this.button_UDP_Save);
            this.tabPage_UDP.Controls.Add(this.label_UDP_Payload);
            this.tabPage_UDP.Controls.Add(this.textBox_UDP_Payload);
            this.tabPage_UDP.Controls.Add(this.label_UDP_Checksum);
            this.tabPage_UDP.Controls.Add(this.label_UDP_Length);
            this.tabPage_UDP.Controls.Add(this.label_UDP_Dst);
            this.tabPage_UDP.Controls.Add(this.textBox_UDP_Checksum);
            this.tabPage_UDP.Controls.Add(this.textBox_UDP_Length);
            this.tabPage_UDP.Controls.Add(this.textBox_UDP_DstPort);
            this.tabPage_UDP.Controls.Add(this.textBox_UDP_SrcPort);
            this.tabPage_UDP.Controls.Add(this.label_UDP_Src);
            this.tabPage_UDP.Controls.Add(this.button_UDP_Send);
            this.tabPage_UDP.Location = new System.Drawing.Point(4, 22);
            this.tabPage_UDP.Name = "tabPage_UDP";
            this.tabPage_UDP.Size = new System.Drawing.Size(439, 328);
            this.tabPage_UDP.TabIndex = 3;
            this.tabPage_UDP.Text = "UDP";
            this.tabPage_UDP.UseVisualStyleBackColor = true;
            // 
            // button_UDP_Save
            // 
            this.button_UDP_Save.Location = new System.Drawing.Point(280, 302);
            this.button_UDP_Save.Name = "button_UDP_Save";
            this.button_UDP_Save.Size = new System.Drawing.Size(75, 23);
            this.button_UDP_Save.TabIndex = 11;
            this.button_UDP_Save.Text = "Save";
            this.button_UDP_Save.UseVisualStyleBackColor = true;
            this.button_UDP_Save.Click += new System.EventHandler(this.button_UDP_Save_Click);
            // 
            // label_UDP_Payload
            // 
            this.label_UDP_Payload.AutoSize = true;
            this.label_UDP_Payload.Location = new System.Drawing.Point(21, 150);
            this.label_UDP_Payload.Name = "label_UDP_Payload";
            this.label_UDP_Payload.Size = new System.Drawing.Size(45, 13);
            this.label_UDP_Payload.TabIndex = 10;
            this.label_UDP_Payload.Text = "Payload";
            // 
            // textBox_UDP_Payload
            // 
            this.textBox_UDP_Payload.Location = new System.Drawing.Point(97, 147);
            this.textBox_UDP_Payload.Name = "textBox_UDP_Payload";
            this.textBox_UDP_Payload.Size = new System.Drawing.Size(307, 20);
            this.textBox_UDP_Payload.TabIndex = 9;
            // 
            // label_UDP_Checksum
            // 
            this.label_UDP_Checksum.AutoSize = true;
            this.label_UDP_Checksum.Location = new System.Drawing.Point(21, 123);
            this.label_UDP_Checksum.Name = "label_UDP_Checksum";
            this.label_UDP_Checksum.Size = new System.Drawing.Size(57, 13);
            this.label_UDP_Checksum.TabIndex = 8;
            this.label_UDP_Checksum.Text = "Checksum";
            // 
            // label_UDP_Length
            // 
            this.label_UDP_Length.AutoSize = true;
            this.label_UDP_Length.Location = new System.Drawing.Point(21, 96);
            this.label_UDP_Length.Name = "label_UDP_Length";
            this.label_UDP_Length.Size = new System.Drawing.Size(43, 13);
            this.label_UDP_Length.TabIndex = 7;
            this.label_UDP_Length.Text = "Length:";
            // 
            // label_UDP_Dst
            // 
            this.label_UDP_Dst.AutoSize = true;
            this.label_UDP_Dst.Location = new System.Drawing.Point(21, 69);
            this.label_UDP_Dst.Name = "label_UDP_Dst";
            this.label_UDP_Dst.Size = new System.Drawing.Size(48, 13);
            this.label_UDP_Dst.TabIndex = 6;
            this.label_UDP_Dst.Text = "Dst Port:";
            // 
            // textBox_UDP_Checksum
            // 
            this.textBox_UDP_Checksum.Location = new System.Drawing.Point(97, 120);
            this.textBox_UDP_Checksum.Name = "textBox_UDP_Checksum";
            this.textBox_UDP_Checksum.Size = new System.Drawing.Size(100, 20);
            this.textBox_UDP_Checksum.TabIndex = 5;
            // 
            // textBox_UDP_Length
            // 
            this.textBox_UDP_Length.Location = new System.Drawing.Point(97, 93);
            this.textBox_UDP_Length.Name = "textBox_UDP_Length";
            this.textBox_UDP_Length.Size = new System.Drawing.Size(100, 20);
            this.textBox_UDP_Length.TabIndex = 4;
            this.textBox_UDP_Length.Text = "8";
            // 
            // textBox_UDP_DstPort
            // 
            this.textBox_UDP_DstPort.Location = new System.Drawing.Point(97, 66);
            this.textBox_UDP_DstPort.Name = "textBox_UDP_DstPort";
            this.textBox_UDP_DstPort.Size = new System.Drawing.Size(100, 20);
            this.textBox_UDP_DstPort.TabIndex = 3;
            this.textBox_UDP_DstPort.Text = "20";
            // 
            // textBox_UDP_SrcPort
            // 
            this.textBox_UDP_SrcPort.Location = new System.Drawing.Point(97, 39);
            this.textBox_UDP_SrcPort.Name = "textBox_UDP_SrcPort";
            this.textBox_UDP_SrcPort.Size = new System.Drawing.Size(100, 20);
            this.textBox_UDP_SrcPort.TabIndex = 2;
            this.textBox_UDP_SrcPort.Text = "10";
            // 
            // label_UDP_Src
            // 
            this.label_UDP_Src.AutoSize = true;
            this.label_UDP_Src.Location = new System.Drawing.Point(21, 42);
            this.label_UDP_Src.Name = "label_UDP_Src";
            this.label_UDP_Src.Size = new System.Drawing.Size(48, 13);
            this.label_UDP_Src.TabIndex = 1;
            this.label_UDP_Src.Text = "Src Port:";
            // 
            // button_UDP_Send
            // 
            this.button_UDP_Send.Location = new System.Drawing.Point(361, 302);
            this.button_UDP_Send.Name = "button_UDP_Send";
            this.button_UDP_Send.Size = new System.Drawing.Size(75, 23);
            this.button_UDP_Send.TabIndex = 0;
            this.button_UDP_Send.Text = "Send";
            this.button_UDP_Send.UseVisualStyleBackColor = true;
            this.button_UDP_Send.Click += new System.EventHandler(this.button_UDP_Send_Click);
            // 
            // tabPage_ICMP
            // 
            this.tabPage_ICMP.Controls.Add(this.label_ICMP_Payload);
            this.tabPage_ICMP.Controls.Add(this.textBox_ICMP_Payload);
            this.tabPage_ICMP.Controls.Add(this.button_ICMP_Save);
            this.tabPage_ICMP.Controls.Add(this.label_ICMP_Chechsum);
            this.tabPage_ICMP.Controls.Add(this.textBox_ICMP_Checksum);
            this.tabPage_ICMP.Controls.Add(this.radioButton_ICMP_EchoReply);
            this.tabPage_ICMP.Controls.Add(this.radioButton_ICMP_EchoRequest);
            this.tabPage_ICMP.Controls.Add(this.button_ICMP_Send);
            this.tabPage_ICMP.Location = new System.Drawing.Point(4, 22);
            this.tabPage_ICMP.Name = "tabPage_ICMP";
            this.tabPage_ICMP.Size = new System.Drawing.Size(439, 328);
            this.tabPage_ICMP.TabIndex = 4;
            this.tabPage_ICMP.Text = "ICMP";
            this.tabPage_ICMP.UseVisualStyleBackColor = true;
            // 
            // button_ICMP_Save
            // 
            this.button_ICMP_Save.Location = new System.Drawing.Point(280, 302);
            this.button_ICMP_Save.Name = "button_ICMP_Save";
            this.button_ICMP_Save.Size = new System.Drawing.Size(75, 23);
            this.button_ICMP_Save.TabIndex = 12;
            this.button_ICMP_Save.Text = "Save";
            this.button_ICMP_Save.UseVisualStyleBackColor = true;
            this.button_ICMP_Save.Click += new System.EventHandler(this.button_ICMP_Save_Click);
            // 
            // label_ICMP_Chechsum
            // 
            this.label_ICMP_Chechsum.AutoSize = true;
            this.label_ICMP_Chechsum.Location = new System.Drawing.Point(23, 93);
            this.label_ICMP_Chechsum.Name = "label_ICMP_Chechsum";
            this.label_ICMP_Chechsum.Size = new System.Drawing.Size(60, 13);
            this.label_ICMP_Chechsum.TabIndex = 11;
            this.label_ICMP_Chechsum.Text = "Checksum:";
            // 
            // textBox_ICMP_Checksum
            // 
            this.textBox_ICMP_Checksum.Location = new System.Drawing.Point(89, 90);
            this.textBox_ICMP_Checksum.Name = "textBox_ICMP_Checksum";
            this.textBox_ICMP_Checksum.Size = new System.Drawing.Size(94, 20);
            this.textBox_ICMP_Checksum.TabIndex = 10;
            // 
            // radioButton_ICMP_EchoReply
            // 
            this.radioButton_ICMP_EchoReply.AutoSize = true;
            this.radioButton_ICMP_EchoReply.Location = new System.Drawing.Point(26, 54);
            this.radioButton_ICMP_EchoReply.Name = "radioButton_ICMP_EchoReply";
            this.radioButton_ICMP_EchoReply.Size = new System.Drawing.Size(80, 17);
            this.radioButton_ICMP_EchoReply.TabIndex = 9;
            this.radioButton_ICMP_EchoReply.TabStop = true;
            this.radioButton_ICMP_EchoReply.Text = "Echo Reply";
            this.radioButton_ICMP_EchoReply.UseVisualStyleBackColor = true;
            // 
            // radioButton_ICMP_EchoRequest
            // 
            this.radioButton_ICMP_EchoRequest.AutoSize = true;
            this.radioButton_ICMP_EchoRequest.Location = new System.Drawing.Point(26, 30);
            this.radioButton_ICMP_EchoRequest.Name = "radioButton_ICMP_EchoRequest";
            this.radioButton_ICMP_EchoRequest.Size = new System.Drawing.Size(93, 17);
            this.radioButton_ICMP_EchoRequest.TabIndex = 8;
            this.radioButton_ICMP_EchoRequest.TabStop = true;
            this.radioButton_ICMP_EchoRequest.Text = "Echo Request";
            this.radioButton_ICMP_EchoRequest.UseVisualStyleBackColor = true;
            // 
            // button_ICMP_Send
            // 
            this.button_ICMP_Send.Location = new System.Drawing.Point(361, 302);
            this.button_ICMP_Send.Name = "button_ICMP_Send";
            this.button_ICMP_Send.Size = new System.Drawing.Size(75, 23);
            this.button_ICMP_Send.TabIndex = 7;
            this.button_ICMP_Send.Text = "Send";
            this.button_ICMP_Send.UseVisualStyleBackColor = true;
            this.button_ICMP_Send.Click += new System.EventHandler(this.button_ICMP_Send_Click);
            // 
            // tabPage_Advanced
            // 
            this.tabPage_Advanced.Controls.Add(this.button_Advanced_Send);
            this.tabPage_Advanced.Controls.Add(this.button_Advanced_Load);
            this.tabPage_Advanced.Controls.Add(this.listBox_Advanced_PreparedPackets);
            this.tabPage_Advanced.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Advanced.Name = "tabPage_Advanced";
            this.tabPage_Advanced.Size = new System.Drawing.Size(439, 328);
            this.tabPage_Advanced.TabIndex = 5;
            this.tabPage_Advanced.Text = "Advanced";
            this.tabPage_Advanced.UseVisualStyleBackColor = true;
            // 
            // button_Advanced_Send
            // 
            this.button_Advanced_Send.Location = new System.Drawing.Point(254, 62);
            this.button_Advanced_Send.Name = "button_Advanced_Send";
            this.button_Advanced_Send.Size = new System.Drawing.Size(75, 23);
            this.button_Advanced_Send.TabIndex = 2;
            this.button_Advanced_Send.Text = "Send";
            this.button_Advanced_Send.UseVisualStyleBackColor = true;
            this.button_Advanced_Send.Click += new System.EventHandler(this.button_Advanced_Send_Click);
            // 
            // button_Advanced_Load
            // 
            this.button_Advanced_Load.Location = new System.Drawing.Point(254, 101);
            this.button_Advanced_Load.Name = "button_Advanced_Load";
            this.button_Advanced_Load.Size = new System.Drawing.Size(75, 23);
            this.button_Advanced_Load.TabIndex = 1;
            this.button_Advanced_Load.Text = "Load";
            this.button_Advanced_Load.UseVisualStyleBackColor = true;
            this.button_Advanced_Load.Click += new System.EventHandler(this.button_Advanced_Load_Click);
            // 
            // listBox_Advanced_PreparedPackets
            // 
            this.listBox_Advanced_PreparedPackets.FormattingEnabled = true;
            this.listBox_Advanced_PreparedPackets.Location = new System.Drawing.Point(53, 46);
            this.listBox_Advanced_PreparedPackets.Name = "listBox_Advanced_PreparedPackets";
            this.listBox_Advanced_PreparedPackets.Size = new System.Drawing.Size(195, 95);
            this.listBox_Advanced_PreparedPackets.TabIndex = 0;
            // 
            // textBox_ICMP_Payload
            // 
            this.textBox_ICMP_Payload.Location = new System.Drawing.Point(77, 117);
            this.textBox_ICMP_Payload.Name = "textBox_ICMP_Payload";
            this.textBox_ICMP_Payload.Size = new System.Drawing.Size(106, 20);
            this.textBox_ICMP_Payload.TabIndex = 13;
            // 
            // label_ICMP_Payload
            // 
            this.label_ICMP_Payload.AutoSize = true;
            this.label_ICMP_Payload.Location = new System.Drawing.Point(23, 120);
            this.label_ICMP_Payload.Name = "label_ICMP_Payload";
            this.label_ICMP_Payload.Size = new System.Drawing.Size(48, 13);
            this.label_ICMP_Payload.TabIndex = 14;
            this.label_ICMP_Payload.Text = "Payload:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 417);
            this.Controls.Add(this.tabControl_Ethernet);
            this.Controls.Add(this.comboBox_Devices);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl_Ethernet.ResumeLayout(false);
            this.tabPage_Ethernet.ResumeLayout(false);
            this.tabPage_Ethernet.PerformLayout();
            this.tabPage_IP.ResumeLayout(false);
            this.tabPage_IP.PerformLayout();
            this.tabPage_TCP.ResumeLayout(false);
            this.tabPage_TCP.PerformLayout();
            this.tabPage_UDP.ResumeLayout(false);
            this.tabPage_UDP.PerformLayout();
            this.tabPage_ICMP.ResumeLayout(false);
            this.tabPage_ICMP.PerformLayout();
            this.tabPage_Advanced.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox_Devices;
        private System.Windows.Forms.TabControl tabControl_Ethernet;
        private System.Windows.Forms.TabPage tabPage_Ethernet;
        private System.Windows.Forms.TabPage tabPage_IP;
        private System.Windows.Forms.TabPage tabPage_TCP;
        private System.Windows.Forms.TabPage tabPage_UDP;
        private System.Windows.Forms.TabPage tabPage_ICMP;
        private System.Windows.Forms.Button button_Eth;
        private System.Windows.Forms.TextBox textBox_Eth_Dest;
        private System.Windows.Forms.TextBox textBox_Eth_Source;
        private System.Windows.Forms.Label label_Eth_Dest;
        private System.Windows.Forms.Label label_Eth_Source;
        private System.Windows.Forms.Label label_Eth_Proto;
        private System.Windows.Forms.ComboBox comboBox_Eth_Proto;
        private System.Windows.Forms.TextBox textBox_Eth_Payload;
        private System.Windows.Forms.Label label_Eth_Payload;
        private System.Windows.Forms.TextBox textBox_IP_Dest;
        private System.Windows.Forms.TextBox textBox_IP_Src;
        private System.Windows.Forms.ComboBox comboBox_IP_Version;
        private System.Windows.Forms.Label label_IP_Dest;
        private System.Windows.Forms.Label label_Source;
        private System.Windows.Forms.Label label_IP_Version;
        private System.Windows.Forms.Label label_IP_HeaderLength;
        private System.Windows.Forms.TextBox textBox_IP_HeaderLength;
        private System.Windows.Forms.Button button_IP_Send;
        private System.Windows.Forms.Label label_IP_TypeofService;
        private System.Windows.Forms.TextBox textBox_IP_TotalLength;
        private System.Windows.Forms.Label label_IP_TotalLength;
        private System.Windows.Forms.CheckBox checkBox_IP_1bit;
        private System.Windows.Forms.TextBox textBox_IP_Identification;
        private System.Windows.Forms.TextBox textBox_IP_Checksum;
        private System.Windows.Forms.ComboBox comboBox_IP_Proto;
        private System.Windows.Forms.TextBox textBox_IP_TTL;
        private System.Windows.Forms.Label label_IP_Identification;
        private System.Windows.Forms.Label label_IP_Checksum;
        private System.Windows.Forms.Label label_IP_Proto;
        private System.Windows.Forms.Label label_IP_TTL;
        private System.Windows.Forms.CheckBox checkBox_IP_X;
        private System.Windows.Forms.CheckBox checkBox_IP_C;
        private System.Windows.Forms.CheckBox checkBox_IP_R;
        private System.Windows.Forms.CheckBox checkBox_IP_T;
        private System.Windows.Forms.CheckBox checkBox_IP_D;
        private System.Windows.Forms.CheckBox checkBox_IP_3bit;
        private System.Windows.Forms.CheckBox checkBox_IP_2bit;
        private System.Windows.Forms.CheckBox checkBox_IP_MF;
        private System.Windows.Forms.CheckBox checkBox_IP_DF;
        private System.Windows.Forms.CheckBox checkBox_IP_Reserved;
        private System.Windows.Forms.Label label_IP_Flags;
        private System.Windows.Forms.TextBox textBox_IP_Payload;
        private System.Windows.Forms.Label label_IP_Payload;
        private System.Windows.Forms.Button button_TCP_Send;
        private System.Windows.Forms.CheckBox checkBox_TCP_Fin;
        private System.Windows.Forms.CheckBox checkBox_TCP_Syn;
        private System.Windows.Forms.CheckBox checkBox_TCP_Reset;
        private System.Windows.Forms.CheckBox checkBox_TCP_Push;
        private System.Windows.Forms.CheckBox checkBox_TCP_Ack;
        private System.Windows.Forms.CheckBox checkBox_TCP_Urgent;
        private System.Windows.Forms.CheckBox checkBox_TCP_ECNEcho;
        private System.Windows.Forms.CheckBox checkBox_TCP_CWR;
        private System.Windows.Forms.CheckBox checkBox_TCP_Nonce;
        private System.Windows.Forms.CheckBox checkBox_TCP_Reserved3;
        private System.Windows.Forms.CheckBox checkBox_TCP_Reserved2;
        private System.Windows.Forms.TextBox textBox_TCP_HeaderLength;
        private System.Windows.Forms.Label label_TCP_HeaderLength;
        private System.Windows.Forms.CheckBox checkBox_TCP_Reserved1;
        private System.Windows.Forms.TextBox textBox_TCP_Urgent;
        private System.Windows.Forms.TextBox textBox_TCP_Checksum;
        private System.Windows.Forms.TextBox textBox_TCP_WinSize;
        private System.Windows.Forms.TextBox textBox_TCP_AckNumber;
        private System.Windows.Forms.TextBox textBox_TCP_SeqNumber;
        private System.Windows.Forms.TextBox textBox_TCP_Dst;
        private System.Windows.Forms.TextBox textBox_TCP_Src;
        private System.Windows.Forms.Label label_TCP_;
        private System.Windows.Forms.Label label_TCP_Checksum;
        private System.Windows.Forms.Label label_TCP_WinSize;
        private System.Windows.Forms.Label label_TCP_AckNumber;
        private System.Windows.Forms.Label label_TCP_SeqNumber;
        private System.Windows.Forms.Label label_TCP_Dst;
        private System.Windows.Forms.Label label_TCP_Src;
        private System.Windows.Forms.TextBox textBox_TCP_Payload;
        private System.Windows.Forms.Label label_TCP_Payload;
        private System.Windows.Forms.Label label_UDP_Checksum;
        private System.Windows.Forms.Label label_UDP_Length;
        private System.Windows.Forms.Label label_UDP_Dst;
        private System.Windows.Forms.TextBox textBox_UDP_Checksum;
        private System.Windows.Forms.TextBox textBox_UDP_Length;
        private System.Windows.Forms.TextBox textBox_UDP_DstPort;
        private System.Windows.Forms.TextBox textBox_UDP_SrcPort;
        private System.Windows.Forms.Label label_UDP_Src;
        private System.Windows.Forms.Button button_UDP_Send;
        private System.Windows.Forms.Label label_UDP_Payload;
        private System.Windows.Forms.TextBox textBox_UDP_Payload;
        private System.Windows.Forms.Button button_ICMP_Send;
        private System.Windows.Forms.RadioButton radioButton_ICMP_EchoReply;
        private System.Windows.Forms.RadioButton radioButton_ICMP_EchoRequest;
        private System.Windows.Forms.Label label_ICMP_Chechsum;
        private System.Windows.Forms.TextBox textBox_ICMP_Checksum;
        private System.Windows.Forms.TabPage tabPage_Advanced;
        private System.Windows.Forms.ListBox listBox_Advanced_PreparedPackets;
        private System.Windows.Forms.Button button_Eth_Save;
        private System.Windows.Forms.Button button_IP_Save;
        private System.Windows.Forms.Button button_TCP_Save;
        private System.Windows.Forms.Button button_UDP_Save;
        private System.Windows.Forms.Button button_ICMP_Save;
        private System.Windows.Forms.Button button_Advanced_Load;
        private System.Windows.Forms.Button button_Advanced_Send;
        private System.Windows.Forms.Label label_IP_FragmentOffset;
        private System.Windows.Forms.TextBox textBox_IP_FragmentOffset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_ICMP_Payload;
        private System.Windows.Forms.TextBox textBox_ICMP_Payload;

    }
}

